'use strict';

function ChatServer(server) 
{
  var io = require('socket.io')(server);
  io.on('connection', function (client)
  {
    console.log('New client');
    client.on('join', function(data)
    {
      client.broadcast.emit('join', client.id);
    });
    
    client.on('msg', function(data)
    {
      console.log('Message: ' + data);
      io.emit('msg', client.id + ': ' + data);
    });
    
    client.on('disconnect', function () 
    {
      client.broadcast.emit('left', client.id);
      console.log('user disconnected');
    });
  });
}
// export the class
module.exports = ChatServer;