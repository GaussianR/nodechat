var Express = require('express'),
    App = Express();
    
var Server = require('http').createServer(App);
var ChatServer = require('./chat');

new ChatServer(Server);

Server.listen(8081, function()
{
  //Set Handlebars.JS as view engine
  App.set('view engine', 'html');
  App.engine('html', require('hbs').__express);
  
  App.get('/', function(req, res)
  {
    res.render('chat/index');
  });
  
  App.get(new RegExp('/resource/([^\\/]+?)/(.+)'), function (req, res)
  {
    res.sendFile(__dirname + '/views/' + req.params[1] + '.' + req.params[0]);
  });
  
  //Set 404 page
  App.use(function(req, res)
  {
    res.status(404).render('errors/404/index');
  });
  
  console.log('Chat server started in http://localhost:8081/');
});