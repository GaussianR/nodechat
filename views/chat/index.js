$( window ).load(function() 
{
  var socket = io.connect('http://localhost:8081');
  socket.on('connect', function (data)
  {
    socket.emit('join', 'Hello World from client');
    
  });
  
  socket.on('msg', function (data)
  {
    $('#messages').append($('<li>').text(data));
  });
  
  socket.on('join', function(data)
  {
    $('#messages').append($('<li class="join-notification">')
                         .text(data + ' joined the server.'));
  });
  
  socket.on('left', function(data)
  {
    $('#messages').append($('<li class="left-notification">')
                           .text(data + ' left the server.'));
  });
  
  $('button').click(function (event)
  {
    socket.emit('msg', $('#m').val());
    $('#m').val('');
    event.preventDefault();
  });
});